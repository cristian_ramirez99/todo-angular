from flask import Flask
from flask import render_template, session, redirect, url_for, request
#from flask_login import LoginManager, login_manager, login_user, logout_user, login_required, current_user
from dotenv import load_dotenv
from datetime import timedelta
import os

#env
load_dotenv()

app = Flask(__name__)

#init session SecretKey
app.secret_key = os.getenv("SECRET_KEY")

# init LoginManager
#login = LoginManager(app)
#login.login_view ='login'

# app.permanent_session_lifetime = timedelta(hours=1)


@app.route('/')
def login():
    if "user_id" in session:
        return redirect(url_for("consulta"))

    return render_template("/UserStory1.html")


@app.route('/historial')
def consulta():
    '''if 'user_id' in session:
        user_id = session["user_id"]'''
    return render_template("/UserStory2.html")
    '''else:
    return redirect(url_for("login"))'''


@app.route('/registrar')
def registrarse():
    return render_template("/UserStory3.html")


@app.route('/mapa')
def localizacion():
    # if 'user_id' in session:
    return render_template("/UserStory4.html")
    '''else:
        return redirect(url_for("login"))'''

@app.route('/admin')
#@login_required
def consultaAdmin():
 #   if current_user.is_admin():
        return render_template("/UserStory5.html")

@app.route('/logout')
def logout():
    #logout_user()
    return redirect(url_for('login'))

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=os.getenv("PORT"), debug=True)
