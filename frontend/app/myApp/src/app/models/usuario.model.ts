export class Usuario {
    constructor(
        public user_id: string,
        public rol: 'Usuario' | 'Administrador',
        public password?: string
    ) { }
}