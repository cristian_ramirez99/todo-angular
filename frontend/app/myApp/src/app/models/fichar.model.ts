export interface fichar {
    pos: { altitud: any, latitud: any }
}
export interface fichaje {
    ok: boolean,
    fichajes: any
}

export class Fichaje {
    constructor(
        public date: any,
        public tipo: 'entrada' | 'salida',
        public pos: { altitud: any, latitud: any },
        public _id?: string,
        public user_id?: string,
        public comentario?: string,

    ) { }
}
export class Pos {
    constructor(
        public altitud: Number,
        public latitud: Number,
    ) { }
}
