import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { PipesModule } from '../pipes/pipes.module';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [
    NavbarComponent,
    HeaderComponent
  ],
  exports: [
    NavbarComponent,
    HeaderComponent,
  ],
  imports: [
    CommonModule,
    PipesModule,
    RouterModule,
    
  ]
})
export class SharedModule { }
