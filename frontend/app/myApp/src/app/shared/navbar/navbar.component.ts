import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Fichaje } from 'src/app/models/fichar.model';
import { FichajeService } from 'src/app/services/fichaje.service';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public fichaje: Fichaje = null;
  public hasFichadoHoy = false;

  constructor(private usuarioService: UsuarioService,
    public fichajeService: FichajeService,
    public router: Router) { }

  ngOnInit(): void {
    this.cargarFichaje();
  }

  logout() {
    this.usuarioService.logout();
  }

  //Carga el ultimoFichaje del usuario si el fichaje es de hoy se muestra
  cargarFichaje() {
    this.fichajeService.cargarUltimoFichaje(this.usuarioService.usuario.user_id)
      .subscribe(fichaje => {
        //Obtenemos el dia actual
        const today = new Date();

        //Si dia actual es el mismo que el ultimoFichaje se muestra el fichaje
        if (today.getDate() == fichaje.date.getDate()
          && today.getMonth() == fichaje.date.getMonth()
          && today.getFullYear() == fichaje.date.getFullYear()) {
          //Actualizamos fichaje
          this.fichaje = fichaje;
          this.hasFichadoHoy = true;
        }
      });
    //Borrar esto solo para pruebas !!!!!!!!!!!!!!!
    this.fichaje = new Fichaje(new Date(2021, 1, 23, 16, 22), 'salida', { altitud: 1, latitud: 1 }, "60071a4458a35c903a179817", 'admin');

    const today = new Date();
    if (today.getDate() == this.fichaje.date.getDate()
      && today.getMonth() == this.fichaje.date.getMonth()
      && today.getFullYear() == this.fichaje.date.getFullYear()) {
      this.hasFichadoHoy = true;
    }
  }

  //Si has fichado ahora o existe un fichaje realizado hoy
  hasFichado() {
    //Si acabas de fichar
    if (this.fichajeService.fichaje !== null) {
      this.fichaje = this.fichajeService.fichaje;
      return true;
      //Si existe un fichajeHoy
    } else if (this.hasFichadoHoy) {
      return true;
    }
    return false
  }

}
