import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Fichaje, Pos } from 'src/app/models/fichar.model';
import { AdminComponent } from 'src/app/pages/admin/admin.component';
import { FechaPipe } from 'src/app/pipes/fecha.pipe';

import { FichajeService } from 'src/app/services/fichaje.service';
import { ModalActualizarFichajeService } from 'src/app/services/modal-actualizar-fichaje.service';
import { ModalService } from 'src/app/services/modal.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-actualizar-fichaje-admin',
  templateUrl: './actualizar-fichaje-admin.component.html',
  styleUrls: ['./actualizar-fichaje-admin.component.css']
})
export class ActualizarFichajeAdminComponent {

  public antiguoFichaje: Fichaje = new Fichaje(new Date(2000, 10, 10, 12, 21), 'salida', { altitud: 41.3, latitud: 2.16 }, "f3as31", "a", "no hay");
  public nuevoFichaje: Fichaje;
  public formSubmitted = false;
  public isAntiguoFichajeActualizado: boolean = false;

  public ficharForm = this.fb.group({
    date: ['', Validators.required],
    hora: ['', Validators.required],
    comentario: ['', Validators.required],
  });

  constructor(private fb: FormBuilder,
    public modalServiceActualizarFichaje: ModalActualizarFichajeService,
    private fichajeService: FichajeService,
    public modalServiceMapa: ModalService,
    public adminComponent: AdminComponent,
    private fechaPipe: FechaPipe) { }

  //Al abrir el modal 
  onInitModal() {
    //Si modal no esta oculto y acabas de abrir el modal
    if (!this.modalServiceActualizarFichaje.ocultarModal && !this.isAntiguoFichajeActualizado) {
      //Actualizo el antiguoFichaje
      this.actualizaAntiguoFichaje();
    }
  }
  actualizaAntiguoFichaje() {
    //Actualizo antiguoFichaje
    this.antiguoFichaje = this.modalServiceActualizarFichaje.fichaje;
    this.isAntiguoFichajeActualizado = true;

    //Esto se podra borrar cuando el CORS funcione
    this.antiguoFichaje.user_id = this.modalServiceActualizarFichaje.user_id;


    const dia = this.fechaPipe.transform(this.antiguoFichaje.date.getDate(), false);
    const mes = this.fechaPipe.transform(this.antiguoFichaje.date.getMonth(), true);
    const año = this.fechaPipe.transform(this.antiguoFichaje.date.getFullYear(), false);
    const hora = this.fechaPipe.transform(this.antiguoFichaje.date.getHours(), false);
    const minutos = this.fechaPipe.transform(this.antiguoFichaje.date.getMinutes(), false);

    //Pongo la informacion en el modal del fichaje a editar.
    this.ficharForm.setValue({
      'date': `${año}-${mes}-${dia}`,
      'hora': `${hora}:${minutos}`,
      'comentario': this.antiguoFichaje.comentario || " "
    });
  }
  //Si el campo esta vacio muestra error de ese campo
  campoNoValido(campo: string) {
    if (this.ficharForm.get(campo).invalid && this.formSubmitted) {
      return true;
    } else {
      return false;
    }
  }
  cerrarModal() {
    if (this.formSubmitted) {
      this.formSubmitted = false;
    }
    this.isAntiguoFichajeActualizado = false;
    this.modalServiceActualizarFichaje.cerrarModal();
  }
  actualizarFichaje() {
    this.formSubmitted = true;

    if (this.ficharForm.invalid) {
      return;
    }

    //Parametros de nuevoFichaje
    const date: Date = this.transformarDate();
    const tipo = this.antiguoFichaje.tipo;
    const _id = this.antiguoFichaje._id;
    const user_id = this.antiguoFichaje.user_id;
    const comentario = this.ficharForm.value.comentario;
    let pos: Pos;

    //pos si es modificado
    if (this.modalServiceMapa.fichaje !== null) {
      pos = this.modalServiceMapa.fichaje.pos;

      //pos si no es modificado
    } else {
      pos = this.antiguoFichaje.pos;
    }

    //Instanciamos nuevoFichaje
    const nuevoFichaje: Fichaje = new Fichaje(date, tipo, pos, _id, user_id, comentario);

    //Hacemos peticion HTTP PUT fichaje
    this.fichajeService.actualizarFichaje(nuevoFichaje)
      .subscribe(resp => {
        //Actualizar si fichaje ok
        //Actualizo el fichaje en admin
        this.adminComponent.actualizarFichajeDeArray(user_id, nuevoFichaje);

        //Cierro el modal
        this.cerrarModal();

        //Muestro alerta de que todo salio bien
        Swal.fire("Accion completada", "Fichaje actualizado con éxito", 'success');
      }, (err) => {
        //Si salta cualquier error
        Swal.fire("Error", "No se pudo realizar la acción", 'error');
      });
    //Borar !!!!!!!!!!
    this.adminComponent.actualizarFichajeDeArray(user_id, nuevoFichaje);

  }

  //Transformo el formato fecha de yyyy-mm-dd y horas:minutos a Date
  transformarDate() {
    var fecha = this.ficharForm.value.date;
    var horas = this.ficharForm.value.hora;

    //Fecha, fecha = yyyy--mm-dd
    var posData = fecha.indexOf("-");
    let año: any = fecha.slice(0, posData);
    año = parseInt(año, 10);

    //fecha = mm-dd
    fecha = fecha.substring(posData + 1, fecha.length);

    posData = fecha.indexOf("-");
    let mes: any = fecha.slice(0, posData);
    mes = parseInt(mes, 10) - 1;

    //fecha == dd
    fecha = fecha.substring(posData + 1, fecha.length);

    let dia: any = fecha;
    dia = parseInt(dia, 10);

    //Hora, horas = horas:minutos
    let posHora = horas.indexOf(":");
    let hora: any = horas.slice(0, posHora);
    hora = parseInt(hora, 10);

    //horas= minutos
    horas = horas.substring(posHora + 1, horas.length);


    let minutos: any = horas;
    minutos = parseInt(minutos, 10);

    return new Date(año, mes, dia, hora, minutos);
  }
  abrirModalMapa() {
    this.modalServiceMapa.abrirModal(this.antiguoFichaje, true);
  }
}
