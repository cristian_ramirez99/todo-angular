import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Fichaje } from 'src/app/models/fichar.model';
import { FichajeService } from 'src/app/services/fichaje.service';
import { ModalActualizarFichajeUsuarioService } from 'src/app/services/modal-actualizar-fichaje-usuario.service';
import Swal from "sweetalert2"
@Component({
  selector: 'app-actualizar-fichaje-usuario',
  templateUrl: './actualizar-fichaje-usuario.component.html',
  styleUrls: ['./actualizar-fichaje-usuario.component.css']
})
export class ActualizarFichajeUsuarioComponent {

  public comentarioActualizado: boolean = false;
  public antiguoFichaje: Fichaje = new Fichaje(new Date(2000, 10, 10, 12, 21), 'salida', { altitud: 41.3, latitud: 2.16 }, "f3as31", "a", "no hay");

  public ficharForm = this.fb.group({
    comentario: ['', Validators.required],
  });

  constructor(public modalActualizarFichajeUsuarioService: ModalActualizarFichajeUsuarioService,
    public fichajeService: FichajeService,
    public fb: FormBuilder) { }

  onInitModal() {
    if (!this.comentarioActualizado) {
      //Actualizar el comentario
      this.comentarioActualizado = true;
      this.antiguoFichaje = this.modalActualizarFichajeUsuarioService.fichaje;

      //Pongo la informacion en el modal del fichaje a editar.
      this.ficharForm.setValue({
        comentario: this.antiguoFichaje.comentario || ' '
      });
    }
  }
  actualizarFichaje() {
    //Instancio un fichaje = antiguoFichaje
    const nuevoFichaje: Fichaje = this.antiguoFichaje;

    //Actualizo el comentario del fichaje
    nuevoFichaje.comentario = this.ficharForm.value.comentario;

    //Hacemos peticion HTTP PUT fichaje
    this.fichajeService.actualizarFichaje(nuevoFichaje)
      .subscribe(resp => {
        //Actualizar si fichaje ok
        //Cerramos modal
        this.cerrarModal();

        //Mostramos alerta al usuario de que todo salio bien
        Swal.fire("Accion completada", "Fichaje actualizado con éxito", 'success');
      }, (err) => {
        //Si salta cualquier error
        Swal.fire("Error", "No se pudo realizar la acción", 'error');
      });
  }
  cerrarModal() {
    this.comentarioActualizado = false;
    this.modalActualizarFichajeUsuarioService.cerrarModal();
  }
}
