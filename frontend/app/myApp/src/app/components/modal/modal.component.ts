import { NgClass } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Fichaje, Pos } from 'src/app/models/fichar.model';
import { MapaService } from 'src/app/services/mapa.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  public fichaje: Fichaje = new Fichaje(new Date(1, 10, 20, 12, 21), 'salida', { altitud: 41.3, latitud: 2.16 });
  public mapaCargado: boolean = false;
  public posTemp: Pos;

  constructor(public mapaService: MapaService,
    public modalService: ModalService) {
  }
  ngOnInit(): void {
    this.mapaService.buildMap();
  }

  //Se inicia cuando se abre el modal
  onInitModal() {
    this.mapaService.removeMarkers();

    //Si el mapa no esta cargado
    if (!this.mapaCargado) {

      //Añadir eventListenerMarkerOnClick si estamos editando la posicion del fichaje
      this.addMarkerOnClick();

      //Obtenemos el fichaje
      this.fichaje = this.modalService.fichaje;
      this.posTemp = this.fichaje.pos;

      //Hacemos fly en la posicion del fichaje
      this.mapaService.flying(this.fichaje.pos.latitud, this.fichaje.pos.altitud);

      //Añdimos marker en la posicion del fichaje
      this.mapaService.addMarker(this.fichaje.pos.latitud, this.fichaje.pos.altitud);
      this.mapaCargado = true;
    }
  }
  cerrarModal() {
    this.mapaCargado = false;
    this.modalService.cerrarModal();
  }

  //Hace un zoom en el fichaje
  fly() {
    this.mapaService.flying(this.fichaje.pos.latitud, this.fichaje.pos.altitud);
  }
  addMarkerOnClick() {
    if (this.modalService.addMarkerOnClick) {
      //Cambiar cursor para hacer añadirMarkers
      this.mapaService.map.getCanvas().style.cursor = 'crosshair';

      //Añade eventListener al clickar
      this.mapaService.map.on('click', (event) => {
        this.mapaService.removeMarkers();

        //console.log('A click event has occurred at ' + event.lngLat);
        //Añade marker onClick
        this.mapaService.addMarker(event.lngLat.lng, event.lngLat.lat);

        //Guardamos nueva geolocalizacion
        this.posTemp = { altitud: event.lngLat.lat, latitud: event.lngLat.lng };
      });
    } else {
      //Cambiar cursor para solo navegar por el mapa
      this.mapaService.map.getCanvas().style.cursor = 'unset';

      //Eliminar el eventListener al clickar
      //this.mapaService.map.off('click');
    }
  }
  aceptar() {
    this.fichaje.pos = this.posTemp;
    this.cerrarModal();
  }
}
