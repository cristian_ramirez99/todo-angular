import { HttpInterceptor } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { UsuarioService } from './usuario.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private injector: Injector) { }

  intercept(req, next) {
    let usuarioService = this.injector.get(UsuarioService)
    let tokenizedReq = req.clone({
      setHeaders: {
        Authorization: `Bearer ${usuarioService.token}`
      }
    })
    return next.handle(tokenizedReq)
  }
}
