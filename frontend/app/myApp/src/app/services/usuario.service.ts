import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Router } from '@angular/router';

import { catchError, map, tap } from 'rxjs/operators'
import { environment } from 'src/environments/environment';

//interfaces
import { LoginForm } from '../interfaces/login-form.interface';
import { RegisterForm } from '../interfaces/register-form.interface';
import { Usuario } from '../models/usuario.model';
import { Observable, of } from 'rxjs';

const base_url = environment.base_url;

declare const gapi: any;

@Injectable({
  providedIn: 'root'
})

export class UsuarioService {

  public auth2: any;
  public usuario: Usuario;

  constructor(private http: HttpClient,
    private router: Router,) { }


  get token(): string {
    return localStorage.getItem('token') || '';
  }
  get role(): 'Administrador' | 'Usuario' {
    return this.usuario.rol;
  }

  get headers() {
    return {
      headers: {
        'x-token': this.token,
        'Authorization': `Bearer ${this.token}`
      }
    }
  }
  guardarLocalStorage(token: string) {
    localStorage.setItem('token', token);
  }

  crearUsuario(formData: RegisterForm) {
    return this.http.post(`${base_url}/user`, formData)
      .pipe(
        tap((resp: any) => {
          this.guardarLocalStorage(resp.token);
        })
      );
  }
  eliminarUsuario(usuario: Usuario) {
    const url = `${base_url}/user/${usuario.user_id}`;
    return this.http.delete(url, this.headers);
  }
  cargarUsuarios() {
    const url = `${base_url}/users`;
    return this.http.get(url, this.headers)
      .pipe(
        map((resp: { 'ok': Boolean, 'users': Usuario[] }) => resp.users
        )
      );
  }
  login(formData: LoginForm) {
    return this.http.post(`${base_url}/login`, formData)
      .pipe(
        tap((resp: any) => {
          this.guardarLocalStorage(resp.token);
        })
      );
  }
  logout() {
    localStorage.removeItem('token');
    this.router.navigateByUrl('/signIn');

  }
  validarToken(): Observable<boolean> {
    return this.http.get(`${base_url}/login`, this.headers)
      .pipe(
        map((resp: any) => {
          const { user_id, rol } = resp.usuario;
          this.usuario = new Usuario(user_id, rol);
          this.guardarLocalStorage(resp.token);

          return true;
        }),
        catchError(error => of(false))
      );
  }
  googleInit() {
    return new Promise(resolve => {
      gapi.load('auth2', () => {
        this.auth2 = gapi.auth2.init({
          //Poner cliente de google!!!!!!!!!
          client_id: '60613601985-1v89lrah087c5r66bgdldgsivc8vhsah.apps.googleusercontent.com',
          cookiepolicy: 'single_host_origin'
        });
        //resolve();
      });
    })
  }

  loginGoogle(token: string) {
    return this.http.post(`$(base_url)/login/google`, { token })
      .pipe(
        tap((resp: any) => {
          this.guardarLocalStorage(resp.token);
        })
      );
  }
}
