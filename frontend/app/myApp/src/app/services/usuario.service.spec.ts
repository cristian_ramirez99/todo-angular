import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { UsuarioService } from 'src/app/services/usuario.service';
import { LoginForm } from '../interfaces/login-form.interface';
import { RegisterForm } from '../interfaces/register-form.interface';
import { environment } from 'src/environments/environment';
import { Usuario } from '../models/usuario.model';

const base_url = environment.base_url;

describe('UsuarioService', () => {
    let usuarioService: UsuarioService;
    let httpTestingController: HttpTestingController;
    let routerTestingModule: RouterTestingModule;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, RouterTestingModule],
            providers: [UsuarioService],
        })
    });

    beforeEach(() => {
        usuarioService = TestBed.get(UsuarioService);
        httpTestingController = TestBed.get(HttpTestingController);
        routerTestingModule = TestBed.get(RouterTestingModule);

        let store = {};
        const mockLocalStorage = {
            getItem: (key: string): string => {
                return key in store ? store[key] : null;
            },
            setItem: (key: string, value: string) => {
                store[key] = `${value}`;
            },
            removeItem: (key: string) => {
                delete store[key];
            },
            clear: () => {
                store = {};
            }
        };
        spyOn(localStorage, 'getItem')
            .and.callFake(mockLocalStorage.getItem);
        spyOn(localStorage, 'setItem')
            .and.callFake(mockLocalStorage.setItem);
        spyOn(localStorage, 'removeItem')
            .and.callFake(mockLocalStorage.removeItem);
        spyOn(localStorage, 'clear')
            .and.callFake(mockLocalStorage.clear);
    });

    afterEach(() => {
        httpTestingController.verify();
    });

    it('Deberia hacer login', () => {
        let loginForm: LoginForm = { nombre: 'cristian', password: '123' };

        usuarioService.login(loginForm)
            .subscribe(resp => {
                expect(loginForm).toBe(resp);
            });

        const req = httpTestingController.expectOne(base_url + '/login')

        expect(req.cancelled).toBeFalsy();
        expect(req.request.responseType).toEqual('json');

        //Ejecuta el subscribe
        req.flush(loginForm);
    });

    it('Deberia de crear un nuevo usuario', () => {
        let registerForm: RegisterForm = { nombre: 'cristian', password: '123', 'rol': 'Usuario' };

        usuarioService.crearUsuario(registerForm)
            .subscribe(resp => {
                expect(registerForm).toBe(resp);
            });
        const req = httpTestingController.expectOne(base_url + '/user');

        expect(req.cancelled).toBeFalsy();
        expect(req.request.responseType).toEqual('json');

        //Ejecuta el subscribe
        req.flush(registerForm);
    });

    it('Deberia obtener token', () => {
        localStorage.setItem('token', 'anothertoken');
        expect(usuarioService.token).toEqual('anothertoken');
    });
    it('Deberia cargar todos los usuarios', () => {
        let usuarios: Usuario[] = [new Usuario('cristian', 'Usuario', '123'), new Usuario('admin', 'Administrador', '12345')];
        usuarioService.cargarUsuarios()
            .subscribe(resp => {
                expect(usuarios).toBe(resp);
            });
        const req = httpTestingController.expectOne(base_url + '/users');

        expect(req.cancelled).toBeFalsy();
        expect(req.request.responseType).toEqual('json');
    });
    /* it('Deberia eliminar un usuario', () => {
         let usuario: Usuario = new Usuario("prueba500", 'Usuario', '50123');
         let registerForm: RegisterForm = { nombre: 'prueba500', password: '50123', rol: 'Usuario' };
 
         //Creamos usuario
         usuarioService.crearUsuario(registerForm)
             .subscribe();
 
         //Eliminamos el usuario
         usuarioService.eliminarUsuario(usuario)
             .subscribe(resp => {
                 expect();
             });
         const req = httpTestingController.expectOne(base_url + '/user');
 
         expect(req.cancelled).toBeFalsy();
         expect(req.request.responseType).toEqual('json');
     }); */

    /*it('Deberia hacer logout', () => {
        localStorage.setItem('token', 'mytoken')

        localStorage.removeItem('token');
    }); */
});
