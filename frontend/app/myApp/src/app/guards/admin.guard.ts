import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { UsuarioService } from '../services/usuario.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor(private usuarioService: UsuarioService,
    private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {

    try {
      if (this.usuarioService.role === 'Administrador') {
        return true;
      } else {
        this.router.navigateByUrl('/dashboard');
        return false;
      }
    } catch (error) {
      this.router.navigateByUrl('/signIn');
    }
  }
}
