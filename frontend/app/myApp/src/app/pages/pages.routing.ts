import { Routes, RouterModule } from '@angular/router'
import { NgModule } from '@angular/core';

//Components
import { PagesComponent } from './pages.component';
import { AdminComponent } from './admin/admin.component';
import { HistorialComponent } from './historial/historial.component';
import { MapaComponent } from './mapa/mapa.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from '../guards/auth.guard';
import { AdminGuard } from '../guards/admin.guard';


const routes: Routes = [
  {
    path: 'dashboard',
    component: PagesComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', component: DashboardComponent },
      { path: 'historial', component: HistorialComponent },
      { path: 'mapa', component: MapaComponent },

      //Rutas de admin
      { path: 'admin', canActivate: [AdminGuard], component: AdminComponent }

    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
