import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Fichaje, Pos } from 'src/app/models/fichar.model';
import { Usuario } from 'src/app/models/usuario.model';
import { FechaPipe } from 'src/app/pipes/fecha.pipe';

import { FichajeService } from 'src/app/services/fichaje.service';
import { UsuarioService } from 'src/app/services/usuario.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  pos: Pos;

  public usuario: Usuario;
  public ultimoFichaje: Fichaje;

  public existeUltimoFichaje = false;

  //Bloquea boton si ya has fichado entrada y salida hoy
  public maxFichajesRealizadosHoy = false;

  //Determina el color del boton
  public proximoFichajeEsEntrada: boolean;

  constructor(private router: Router,
    private fichajeService: FichajeService,
    private usuarioService: UsuarioService,
    private fechaPipe: FechaPipe) {
    this.usuario = usuarioService.usuario;
  }
  ngOnInit(): void {
    this.cargatUltimoFichaje();

    //Obtenemos la posicion actual del usuario, lo hacemos ahora para no tener problemas con async
    this.getLocation();
  }

  //Hacemos peticion POST de fichaje
  async fichar() {
    //Obtenemos fecha actual
    const date = new Date();

    //Obtenemos el nuevoTipo
    const tipo = this.getNewTipo();

    //Instanciamos el nuevo fichaje
    const fichaje = new Fichaje(date, tipo, this.pos, '', this.usuario.user_id);

    //Peticion POST nuevoFichaje
    this.fichajeService.fichar(fichaje)
      .subscribe(resp => {
        //Actualizo el ultimoFichaje 
        this.ultimoFichaje = fichaje;
        this.existeUltimoFichaje = true;

        //Si fichaje tipo salida no se puede fichar mas hoy
        if (fichaje.tipo === "salida") {
          this.maxFichajesRealizadosHoy = true;
          //Cambiamos el color del boton
        } else {
          this.proximoFichajeEsEntrada = false;
        }

        const hora = this.fechaPipe.transform(fichaje.date.getHours(), false);
        const minutos = this.fechaPipe.transform(fichaje.date.getMinutes(), false);

        //Muestra alerta confirmando que has fichado
        Swal.fire("Fichaje realizado", `Has fichado la ${tipo} a las ${hora}:${minutos}`, 'success');
      }, (err) => {
        Swal.fire("Error", "Algo inesperado sucedio", 'error');
      });

    //Borrar esto !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //Si fichaje tipo salida no se puede fichar mas hoy
    if (fichaje.tipo === "salida") {
      //Bloqueamos boton
      this.maxFichajesRealizadosHoy = true;
    } else {
      this.proximoFichajeEsEntrada = false;
    }
  }
  //Devuelve true si eres Administrador
  eresAdmin() {
    if (this.usuarioService.role === 'Administrador') {
      return true;
    } else {
      return false;
    }
  }

  //Obtenemos la posicion actual del usuario
  async getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        if (position) {
          this.pos = { altitud: position.coords.latitude, latitud: position.coords.longitude }
        }
      },
        (error) => console.log(error));
    } else {
      Swal.fire("Error", "Geolocalizacion no esta permitida en este navegador", "error");
    }
  }
  //Muestra el modal para asegurar que el usuario quiere fichar
  async mostrarModalFichar() {
    Swal.fire({
      title: `Quieres fichar la ${this.getNewTipo()}?`,
      text: "Una vez hecho no podrás deshacerlo",
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Fichar'
    }).then((result) => {
      if (result.isConfirmed) {
        //Hacemos peticion POST fichar, si usuario confirma
        this.fichar();
      }
    })
  }
  //Peticion GET del ultimoFichaje realizado 
  cargatUltimoFichaje() {
    this.fichajeService.cargarUltimoFichaje(this.usuario.user_id)
      .subscribe(fichaje => {
        //Actualizo ultimoFichaje
        this.ultimoFichaje = fichaje;
        this.existeUltimoFichaje = true;

        //Obtenemos el dia actual
        const today = new Date();

        //Determina el color del btn fichar
        if (fichaje.tipo === 'entrada') {
          this.proximoFichajeEsEntrada = false;
        } else {
          this.proximoFichajeEsEntrada = true;
        }
        //Si dia actual es el mismo que el ultimoFichaje y es de tipo salida, ya no se pueden hacer mas fichajes hoy
        if (today.getDate() == fichaje.date.getDate()
          && today.getMonth() == fichaje.date.getMonth()
          && today.getFullYear() == fichaje.date.getFullYear()
          && fichaje.tipo === 'salida') {
          //Bloqueamos el boton
          this.maxFichajesRealizadosHoy = true;
        }
      });

    //Para pruebas, borrar !!!!
    this.ultimoFichaje = new Fichaje(new Date(2021, 1, 23, 15, 22), 'salida', { altitud: 1, latitud: 1 }, "60071a4458a35c903a179817", 'admin');
    this.existeUltimoFichaje = true;

    //Obtenemos el dia actual
    const today = new Date();

    //Si dia actual es el mismo que el ultimoFichaje y es de tipo salida, ya no se pueden hacer mas fichajes hoy
    if (today.getDate() == this.ultimoFichaje.date.getDate()
      && today.getMonth() == this.ultimoFichaje.date.getMonth()
      && today.getFullYear() == this.ultimoFichaje.date.getFullYear()
      && this.ultimoFichaje.tipo === 'salida') {
      this.maxFichajesRealizadosHoy = true;
    }
    //Determina el color del btn fichar
    if (this.ultimoFichaje.tipo === 'entrada') {
      this.proximoFichajeEsEntrada = false;
    } else {
      this.proximoFichajeEsEntrada = true;
    }
    ///////////////
  }
  //Si el ultimoFichaje era de entrada te devuelve salida y viceversa
  getNewTipo() {
    return this.ultimoFichaje.tipo === 'entrada' ? 'salida' : 'entrada';
  }
}
