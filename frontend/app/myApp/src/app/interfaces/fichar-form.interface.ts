import { StringifyOptions } from "querystring";

export interface FicharForm {
    date: string;
    hora: string;
    tipo: 'entrada' | 'salida';
    pos: { altitud: any, latitud: any };
    comentario?: string;
}