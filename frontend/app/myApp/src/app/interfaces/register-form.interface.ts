export interface RegisterForm {
    nombre: string;
    password: string;
    rol: 'Usuario' | 'Administrador';
}