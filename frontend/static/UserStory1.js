var buttonContador = 0;
var selectedButton;

function loadEvents() {
    getGeolocalizacion();
    document.getElementById("ficharButton").addEventListener('click', JSONAuth);
    document.getElementById("historialButton").addEventListener('click', JSONAuth);
}
function alertOk(tipo) {
    alert("Has fichado la " + tipo + " correctamente");
}
function alertError() {
    alert("Error en el usuario o en la contraseña");
}
function getGeolocalizacion() {
    var options = { timeout: 30000 };

    function error(err) {
        console.warn('ERROR(' + err.code + '): ' + err.message);
    }
    navigator.geolocation.getCurrentPosition(function (position) {
        document.getElementById("altitud").value = position.coords.longitude;
        document.getElementById("latitud").value = position.coords.latitude;
    }, error, options);
}
function getCurrentDate() {
    var currentDate = new Date();
    var dateTime = currentDate.getDate() + "/" +
        (currentDate.getMonth() + 1) + "/" +
        currentDate.getFullYear() + " " +
        currentDate.getHours() + ":" +
        currentDate.getMinutes();

    return dateTime;
}
function changeButtonColour() {
    var ficharButton = document.getElementById("ficharButton");

    if (buttonContador % 2 == 0) {
        alertOk("entrada");
        ficharButton.style.backgroundColor = "blue";
        ficharButton.textContent = "Fichar Salida";

    } else {
        alertOk("salida");
        ficharButton.style.backgroundColor = "green";
        ficharButton.textContent = "Fichar Entrada";
    }
    buttonContador++;
}
function getTipo() {
    if (buttonContador % 2 == 0) {
        return "salida";
    } else
        return "entrada";
}

function loadHTMLHistorial() {
    var user_id = document.getElementById("inputUsername").value;
    window.location.replace("http://localhost:5000/historial?user_id=" + user_id);
}

function fichar() {
    postJSONFichaje();
    changeButtonColour();
}

function sendData() {
    var user_id = document.getElementById("inputUsername").value;

    var altitud = document.getElementById("altitud").value;
    var latitud = document.getElementById("latitud").value;

    var data = JSON.stringify({
        "user_id": user_id,
        "date": getCurrentDate(),
        "tipo": getTipo(),
        "pos": { "altitud": altitud, "latitud": latitud }
    });
    return data;

}
function sendPassword() {
    var user_id = document.getElementById("inputUsername").value;
    var password = document.getElementById("inputPassword").value;

    var hash = calcMD5(password);

    var data = JSON.stringify({
        "user_id": user_id,
        "password": hash
    });
    return data

}
function errorAuth(messageError) {
    var container = document.getElementById("container");
    var div = document.createElement("div");
    div.setAttribute("id", "errorAuth");

    div.innerHTML = "<p>" + messageError + "</p>";

    container.appendChild(div);

}
function proccessAuth() {
    if (this.readyState == 4 && this.status == 200) {
        var obj = JSON.parse(this.responseText);
        var containerError = document.getElementById("errorAuth");

        if(containerError){
            console.log("errorAuth")
            containerError.remove();
        }
        if (obj.ok) {
            if (selectedButton === "ficharButton") {
                fichar();
            } else {
                loadHTMLHistorial();
            }
        } else {
            errorAuth("Contraseña incorrecta");
        }
    }
}
function JSONAuth() {
    if (this.value === "ficharButton") {
        selectedButton = "ficharButton";
    } else {
        selectedButton = "historialButton";
    }
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = proccessAuth;
    xhr.open("POST", "http://localhost:5001/api/login", true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(sendPassword());
}
function postJSONFichaje() {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:5001/api/fichaje", true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(sendData());
}
