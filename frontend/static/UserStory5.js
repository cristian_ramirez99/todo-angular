var buttonPressed;

var currentMarkers = []

function loadEvents() {
    loadAllUsersJSON();
    document.getElementById("mapaButton").addEventListener('click', loadSelectedUsers);
    document.getElementById("fichajesButton").addEventListener('click', loadSelectedUsers);
}

function loadAllUsersJSON() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = processUsers;
    xhr.open("GET", "http://localhost:5001/api/users", true);
    xhr.send();
}

function processUsers() {
    if (this.readyState == 4 && this.status == 200) {
        var e = document.getElementById("usuarios");
        var obj = JSON.parse(this.responseText);

        for (let i = 0; obj.length > i; i++) {
            var user_id = obj[i].user_id;
            e.innerHTML +=
                "<input class='form-check-input' type='checkbox' value=" + user_id + ">" +
                "<label class='form-check-label' for=" + user_id + ">" + user_id + " </label> " +
                "</br>"

        }
    }
}

function loadSelectedUsers() {
    if (buttonPressed !== this.value) {
        buttonPressed = this.value;
    }
    createElements();
    var input = document.getElementsByTagName("input");

    for (let i = 0; input.length > i; i++) {
        if (input[i].checked) {
            var user_id = input[i].value;
            loadFichajes(user_id);
        }
    }
}
function loadFichajes(user_id) {
    var xhr = new XMLHttpRequest();

    if (buttonPressed === "mapaButton") {
        xhr.onreadystatechange = processMapa;
    } else {
        xhr.onreadystatechange = processFichajes;
    }
    xhr.open("GET", "http://localhost:5001/api/fichajes/" + user_id, true);
    xhr.send();
}
function processFichajes() {
    if (this.readyState == 4 && this.status == 200) {
        var obj = JSON.parse(this.responseText);

        if (obj.length > 0) {

            var user_id = obj[0].user_id;
            createTableFichaje(user_id);

            var e = document.getElementById(user_id);

            for (let i = 0; obj.length > i; i++) {
                //Data JSON
                var fecha = obj[i].date;
                var tipo = obj[i].tipo;
                var altitud = obj[i].pos.altitud;
                var latitud = obj[i].pos.latitud;

                //Slice fecha 
                let posData = fecha.indexOf("/");
                var dia = fecha.slice(0, posData);
                fecha = fecha.substring(posData + 1, fecha.length);

                posData = fecha.indexOf("/");
                var mes = fecha.slice(0, posData);
                fecha = fecha.substring(posData + 1, fecha.length);

                posData = fecha.indexOf(" ");
                var año = fecha.slice(0, posData);
                fecha = fecha.substring(posData + 1, fecha.length);

                posData = fecha.indexOf(":");
                var hora = fecha.slice(0, posData);
                fecha = fecha.substring(posData + 1, fecha.length);

                var minutos = fecha.slice(0, fecha.length);

                if (minutos < 10) {
                    minutos = 0 + minutos;
                }

                //Write fichaje in HTML
                e.innerHTML += "<tr>" +
                    "<td>" + tipo + "</td>" +
                    "<td>" + dia + "</td>" +
                    "<td>" + mes + "</td>" +
                    "<td>" + año + "</td>" +
                    "<td>" + hora + ":" + minutos + "</td>" +
                    "<td> <a href='http://localhost:5000/mapa?alt=" + altitud + "&lat=" + latitud + "'> <img src='../media/mapIcon.jpg' alt='imagen mapa' width='40px' height='30px'/> </a> </td>" +
                    "</tr>";
            }
        }
    }
}


function processMapa() {
    if (this.readyState == 4 && this.status == 200) {
        var obj = JSON.parse(this.responseText);
        if (obj.length > 0) {
            for (let i = 0; obj.length > i; i++) {
                var altitud = obj[i].pos.altitud;
                var latitud = obj[i].pos.latitud;

                addMarker(altitud, latitud);
            }
        }
    }

}

function createTableFichaje(user_id) {
    var table = document.createElement("div");
    var container = document.getElementById("containerFichajes");

    table.innerHTML +=
        "<h1>" + user_id + "</h1>" +
        "<div class='table-responisve fichajes'>" +
        "<table id='" + user_id + "'class='table table-bordered table-hover'>" +
        "<thead class='thead-dark'>" +
        "<tr>" +
        "<th>Tipo</th>" +
        "<th>Dia</th>" +
        "<th>Mes</th>" +
        "<th>Año</th>" +
        "<th>Hora</th>" +
        "<th>Localización</th>" +
        "</tr>" +
        "</table>" +
        "</div>";

    container.append(table);
}
function createElements() {
    var mapa = document.getElementById("mapa");
    var fichajes = document.getElementById("containerFichajes");

    if (buttonPressed === "mapaButton") {
        if (!mapa) {
            createMap();
        } else {
            clearMarkers();
        }
        if (typeof (fichajes) != 'undefined' && fichajes != null) {
            deleteElement("containerFichajes");
        }
    } else if (buttonPressed === "fichajesButton") {
        if (typeof (mapa) != 'undefined' && mapa != null) {
            deleteElement("mapa");
        } if (typeof (fichajes) != 'undefined' && fichajes != null) {
            deleteElement("containerFichajes");
        }
        createDivOfFichajes();
    }
}
function deleteElement(id) {
    var e = document.getElementById(id);

    if (!e) {
        alert("No se puede borrar el elemento bobo");
    } else {
        parent = e.parentNode;
        parent.removeChild(e);
    }
}
function createDivOfFichajes() {
    var table = document.createElement("div");
    table.innerHTML +=
        "<div id='containerFichajes' class='container'></div>";
    document.body.appendChild(table);

}

function createMap() {
    //Create the element map
    var eMapa = document.createElement("div");
    eMapa.style.width = "30%";
    eMapa.style.height = "400px";
    eMapa.setAttribute("id", "mapa");
    eMapa.setAttribute("class", "float-right");
    document.body.appendChild(eMapa);

    var altitudCat = 1.57634927;
    var latitudCat = 41.78424664;

    mapboxgl.accessToken = 'pk.eyJ1IjoiY3Jpc3RpYW5yYW1pcmV6OTkiLCJhIjoiY2toNHBzbHgxMDBqMzJ2cDV0aTNraGo3YyJ9.0OsfMH_GY07BxQ7xks3dYA';
    map = new mapboxgl.Map({
        container: 'mapa',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [altitudCat, latitudCat],
        zoom: 6.7

    });

}
function clearMarkers() {
    if (currentMarkers !== null) {
        for (let i = 0; currentMarkers.length > i; i++) {
            currentMarkers[i].remove();
        }
    }
    currentMarkers = [];
}

function addMarker(altitud, latitud) {
    var marker = new mapboxgl.Marker({})
        .setLngLat([altitud, latitud])
        .addTo(map);

    currentMarkers.push(marker);
}