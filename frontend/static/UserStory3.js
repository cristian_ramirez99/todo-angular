function loadEvents() {
    document.getElementById("registrarButton").addEventListener('click', postUserJSON);
}
function alertOk() {
    alert("Se ha creado el usuario correctamente");
}
function alertError() {
    alert("Debes añadir los dos campos para crear un usuario");
}

function sendData() {
    var user_id = document.getElementById("inputUsername").value;
    var password = document.getElementById("inputPassword").value;
    var rol = document.getElementById("rol").value;
    console.log(rol);
    var hash = calcMD5(password);


    data = JSON.stringify({ "user_id": user_id, "password": hash, "rol": rol });
    alertOk();

    return data;
}

function postUserJSON() {
    var user_id = document.getElementById("inputUsername").value;
    var password = document.getElementById("inputPassword").value;

    if (user_id.length != 0 || password.length != 0) {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "http://localhost:5001/api/user", true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(sendData());
    } else {
        alertError();
    }
}