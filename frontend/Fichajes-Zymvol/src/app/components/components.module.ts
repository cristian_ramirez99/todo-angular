import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalComponent } from './modal/modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActualizarFichajeAdminComponent } from './modal/actualizar-fichaje-admin/actualizar-fichaje-admin.component';
import { ActualizarFichajeUsuarioComponent } from './modal/actualizar-fichaje-usuario/actualizar-fichaje-usuario.component';
import { FiltrarFichajesComponent } from './modal/filtrar-fichajes/filtrar-fichajes.component';
import { PipesModule } from '../pipes/pipes.module';
import { FechaPipe } from '../pipes/fecha.pipe';



@NgModule({
  declarations: [
    ModalComponent,
    ActualizarFichajeAdminComponent,
    ActualizarFichajeUsuarioComponent,
    FiltrarFichajesComponent,
  ],
  exports: [
    ModalComponent,
    ActualizarFichajeAdminComponent,
    ActualizarFichajeUsuarioComponent,
    FiltrarFichajesComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule,
  ],
  providers: [FechaPipe]
})
export class ComponentsModule { }
