import { Component, OnInit, Optional } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Fichaje } from 'src/app/models/fichar.model';
import { AdminComponent } from 'src/app/pages/admin/admin.component';
import { HistorialComponent } from 'src/app/pages/historial/historial.component';
import { FechaPipe } from 'src/app/pipes/fecha.pipe';
import { FichajeService } from 'src/app/services/fichaje.service';
import { ModalFiltrarFichajesService } from 'src/app/services/modal-filtrar-fichajes.service';

@Component({
  selector: 'app-filtrar-fichajes',
  templateUrl: './filtrar-fichajes.component.html',
  styleUrls: ['./filtrar-fichajes.component.css']
})
export class FiltrarFichajesComponent implements OnInit {

  public user_id: string;

  public fechaInicial: Date;
  public fechaFinal: Date;

  //Esto es para pruebas !!!!!!!
  public respFichaje: Fichaje[] = [];

  public formSubmitted: boolean = false;
  periodoActualizado: boolean = false;

  public filtrarFichajesForm = this.fb.group({
    dateInicial: ['', Validators.required],
    dateFinal: ['', Validators.required]
  });
  constructor(private fb: FormBuilder,
    public modalFiltrarFichajesService: ModalFiltrarFichajesService,
    public fichajeService: FichajeService,
    private router: Router,
    private fechaPipe: FechaPipe,
    @Optional() private adminComponent: AdminComponent,
    @Optional() private historialComponent: HistorialComponent,
  ) { }

  ngOnInit(): void {
    var date = new Date();

    //Por default fechaInicial=hace un mes y fechaFinal=hoy
    this.fechaInicial = new Date(date.getFullYear(), date.getMonth() - 1, date.getDate());
    this.fechaFinal = new Date(date.getFullYear(), date.getMonth(), date.getDate());
  }
  onInitModal() {
    if (!this.periodoActualizado) {
      this.periodoActualizado = true;
      this.user_id = this.modalFiltrarFichajesService.user_id;

      const diaInicial = this.fechaPipe.transform(this.fechaInicial.getDate(), false);
      const mesInicial = this.fechaPipe.transform(this.fechaInicial.getMonth(), true);
      const añoInicial = this.fechaPipe.transform(this.fechaInicial.getFullYear(), false);

      const diaFinal = this.fechaPipe.transform(this.fechaFinal.getDate(), false);
      const mesFinal = this.fechaPipe.transform(this.fechaFinal.getMonth(), true);
      const añoFinal = this.fechaPipe.transform(this.fechaFinal.getFullYear(), false);

      //Pongo la informacion en el modal del anterior periodo seleccionado
      this.filtrarFichajesForm.setValue({
        'dateInicial': `${añoInicial}-${mesInicial}-${diaInicial}`,
        'dateFinal': `${añoFinal}-${mesFinal}-${diaFinal}`,
      });
    }
  }

  //Los fichajes se cargan en el Component historial o admin
  cargarFichajes() {
    if (!this.formSubmitted) {
      this.formSubmitted = true;
    }

    //Si el form invalido no hagas nada 
    if (this.filtrarFichajesForm.invalid || !this.periodoValido()) {
      return;
    }

    //Devuelve las fechas en formato Date
    const dateInicial = this.transformarDate('dateInicial');
    const dateFinal = this.transformarDate('dateFinal');

    //Actualizamos las fechas
    this.fechaInicial = dateInicial;
    this.fechaFinal = dateFinal

    //Si estamos en la url historial
    if (this.router.url.includes('historial')) {
      this.historialComponent.cargarFichajesPeriodo(dateInicial, dateFinal);

      //Si estamos en la url admin
    } else if (this.router.url.includes('admin')) {
      this.adminComponent.cargarFichajesPeriodo(dateInicial, dateFinal);
    }
    //Cerramos el modal
    this.cerrarModal();
  }

  //Transformo el formato fecha de yyyy-mm-dd a Date
  transformarDate(campo: string) {
    var fecha = this.filtrarFichajesForm.get(campo).value;

    //Fecha, fecha = yyyy--mm-dd
    var posData = fecha.indexOf("-");
    let año: any = fecha.slice(0, posData);
    año = parseInt(año, 10);

    //fecha = mm-dd
    fecha = fecha.substring(posData + 1, fecha.length);

    posData = fecha.indexOf("-");
    let mes: any = fecha.slice(0, posData);
    mes = parseInt(mes, 10) - 1;

    //fecha= dd
    fecha = fecha.substring(posData + 1, fecha.length);

    let dia: any = fecha;
    dia = parseInt(dia, 10);

    return new Date(año, mes, dia);
  }
  //Si el campo esta vacio muestra error de ese campo
  campoNoValido(campo: string) {
    return this.filtrarFichajesForm.get(campo).invalid;
  }

  //Si es  true el periodo es valido
  periodoValido() {
    return this.filtrarFichajesForm.value.dateFinal > this.filtrarFichajesForm.value.dateInicial;
  }
  cerrarModal() {
    this.periodoActualizado = false;
    this.modalFiltrarFichajesService.cerrarModal();
  }
}
