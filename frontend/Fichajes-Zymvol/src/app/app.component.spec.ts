import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { Fichaje } from './models/fichar.model';

/*let fichajes: Fichaje = {
  "_id": "60256e1daa0aea312a421438",
  "user_id": "admin",
  "date": "2021-02-11T17:49:12.526Z",
  "tipo": "entrada",
  "pos": {
    "altitud": 41.4525333,
    "latitud": 2.2166593
  }
};

let fichajesResponse: any = {
  "uid": {
    "_id": "60256e1daa0aea312a421438"
  },
  "user_id": "admin",
  "date": "2021-02-11T17:49:12.526Z",
  "tipo": "entrada",
  "pos": {
    "altitud": 41.4525333,
    "latitud": 2.2166593
  }
};
const { uid: { _id }, user_id } = fichajesResponse;
*/


describe('AppComponent', () => {

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'Fichajes-Zymbol'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('Fichajes-Zymbol');
  });

  /*it("is the same", () => {
    console.log(_id);
    expect(fichajes).toEqual(fichajesResponse);
  }); */
});
