import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//Modulos
import { AdminComponent } from './admin/admin.component';
import { HistorialComponent } from './historial/historial.component';
import { MapaComponent } from './mapa/mapa.component';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { ComponentsModule } from '../components/components.module';
import { PipesModule } from '../pipes/pipes.module';
import { FechaPipe } from '../pipes/fecha.pipe';

@NgModule({
  declarations: [
    AdminComponent,
    HistorialComponent,
    MapaComponent,
    PagesComponent,
    DashboardComponent
  ],
  exports: [
    AdminComponent,
    HistorialComponent,
    MapaComponent,
    PagesComponent
  ],
  imports: [
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule,
    ComponentsModule,
    PipesModule,
  ],
  providers: [FechaPipe]
})
export class PagesModule { }
