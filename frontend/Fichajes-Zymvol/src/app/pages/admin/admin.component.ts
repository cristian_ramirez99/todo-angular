import { Component, OnInit } from '@angular/core';
import { Fichaje } from 'src/app/models/fichar.model';
import { Usuario } from 'src/app/models/usuario.model';
import { FechaPipe } from 'src/app/pipes/fecha.pipe';
import { EstadisticasService } from 'src/app/services/estadisticas.service';
import { FichajeService } from 'src/app/services/fichaje.service';
import { MapaService } from 'src/app/services/mapa.service';
import { ModalActualizarFichajeService } from 'src/app/services/modal-actualizar-fichaje.service';
import { ModalFiltrarFichajesService } from 'src/app/services/modal-filtrar-fichajes.service';
import { ModalService } from 'src/app/services/modal.service';
import { UsuarioService } from 'src/app/services/usuario.service';
import Swal from 'sweetalert2';

new Date(1, 2);
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  public usuariosTotales: Usuario[] = [];
  public usuariosSeleccionados: string[] = [];

  public fichajes = new Map<string, Fichaje[]>();
  public horasTotales = new Map<string, any>();
  public minutosTotales = new Map<string, any>();

  public dateInicial: Date;
  public dateFinal: Date;

  public mostrarFichajes: boolean = false;
  public mostrarMapa: boolean = false;
  public isfichajeActualizado: boolean = false;


  constructor(private fichajeService: FichajeService,
    private usuarioService: UsuarioService,
    private mapaService: MapaService,
    private modalService: ModalService,
    private modalActualizarFichajeService: ModalActualizarFichajeService,
    private modalFiltrarFichajesService: ModalFiltrarFichajesService,
    private fechaPipe: FechaPipe,
    public estadisticasService: EstadisticasService) {
  }

  ngOnInit(): void {
    this.cargarUsuarios();

    //Por default fechaInicial=hace un mes y fechaFinal=hoy
    var date = new Date();

    //Hoy
    this.dateInicial = new Date(date.getFullYear(), date.getMonth() - 1, date.getDate());

    //Hace un mes
    this.dateFinal = new Date(date.getFullYear(), date.getMonth(), date.getDate());

    const nuevoArray = [new Fichaje(new Date(1800, 10, 5, 20, 10), 'entrada', { altitud: 23, latitud: 2 }, "60071a4458a35c903a179816", 'admin', "Doña Uzeada de Ribera Maldonado de Bracamonte y Anaya era baja, rechoncha, abigotada. Ya no existia razon para llamar talle al suyo. palmipedos domesticos. Sortijas celestes y azules le ahorcaban las"),
    new Fichaje(new Date(1700, 10, 2, 15, 22), 'salida', { altitud: 1, latitud: 1 }, "60071a4458a35c903a179817", 'admin'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'entrada', { altitud: 2, latitud: 3 }, "opi4390ab", 'admin'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'salida', { altitud: 3, latitud: 4 }, "opi4390abc", 'admin'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'entrada', { altitud: 4, latitud: 5 }, "opi4390abd", 'admin'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'salida', { altitud: 5, latitud: 6 }, "opi4390abe", 'admin'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'entrada', { altitud: 6, latitud: 7 }, "opi4390abf", 'admin'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'salida', { altitud: 7, latitud: 8 }, "opi4390abg", 'admin'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'entrada', { altitud: 8, latitud: 9 }, "opi4390abh", 'admin'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'salida', { altitud: 9, latitud: 10 }, "opi4390abj", 'admin'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'entrada', { altitud: 10, latitud: 11 }, "opi4390abjq", 'admin'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'salida', { altitud: 11, latitud: 12 }, "opi4390abaasd", 'admin')];


    const nuevoArray2 = [new Fichaje(new Date(1, 1, 1, 1, 1), 'entrada', { altitud: 23, latitud: 2 }, "opi4390a11", 'cristian'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'salida', { altitud: 1, latitud: 2 }, "opi4390a2", 'cristian'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'entrada', { altitud: 2, latitud: 2 }, "opi4390a23", 'cristian'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'salida', { altitud: 3, latitud: 3 }, "opi4390a24", 'cristian'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'entrada', { altitud: 4, latitud: 4 }, "opi4390a25", 'cristian'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'salida', { altitud: 4, latitud: 4 }, "opi4390a26", 'cristian'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'entrada', { altitud: 5, latitud: 5 }, "opi4390a27", 'cristian'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'salida', { altitud: 6, latitud: 6 }, "opi4390a28", 'cristian'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'entrada', { altitud: 7, latitud: 7 }, "opi4390a29", 'cristian'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'salida', { altitud: 7, latitud: 7 }, "opi4390a20", 'cristian'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'entrada', { altitud: 8, latitud: 8 }, "opi4390a113", 'cristian'),
    new Fichaje(new Date(1, 1, 1, 1, 1), 'salida', { altitud: 9, latitud: 9 }, "opi4390a12", 'cristian')];

    this.fichajes.set("admin", nuevoArray);
    this.fichajes.set("cristian", nuevoArray2);

  }
  //Carga el mapa con las localizaciones de usuariosSeleccionado
  cargarMapa() {
    //Peticion GET fichajes
    this.cargarFichajesPeriodo(this.dateInicial, this.dateFinal);

    //Construye el mapa
    this.mapaService.buildMap();

    //Añde una punto por cada fichaje
    for (const fichajes of this.fichajes.values()) {
      fichajes.forEach(fichaje => {
        this.mapaService.addMarker(fichaje.pos.latitud, fichaje.pos.altitud);
      });
    }
  }
  //Peticion GET usuarios
  cargarUsuarios() {
    this.usuarioService.cargarUsuarios()
      .subscribe((usuarios: Usuario[]) => {
        this.usuariosTotales = usuarios;
      });
  }
  //Actualiza array de usuariosSeleccionados, onChange 
  cargarUsuariosSeleccionado(e: any, usuario: Usuario) {
    if (e.target.checked) {
      this.usuariosSeleccionados.push(usuario.user_id);
    } else {
      this.usuariosSeleccionados = this.usuariosSeleccionados.filter(m => m != usuario.user_id);
    }
  }

  //Muestra el mapa si mostrarrMapa == true
  activarMostrarMapa() {
    if (!this.mostrarMapa) {
      this.mostrarMapa = true;
    }
    if (this.mostrarFichajes) {
      this.mostrarFichajes = false;
    }
  }
  //Muestra tabla con fichajes si mostrarFichajes == true
  activarMostrarFichajes() {
    if (!this.mostrarFichajes) {
      this.mostrarFichajes = true;
    }
    if (this.mostrarMapa) {
      this.mostrarMapa = false;
    }
  }

  abrirModal(fichaje: Fichaje) {
    this.modalService.abrirModal(fichaje, false);
  }
  eliminarFichaje(user_id: string, fichaje_id: string) {

    this.fichajeService.eliminarFichaje(fichaje_id)
      .subscribe(resp => {
        //Actualiza array si ok
        this.eliminarFichajeDeArray(user_id, fichaje_id);

        //Mostramos alerta al usuario conforme todo salio bien
        Swal.fire("Fichaje eliminado", "Fichaje eliminado", 'success');
      }
      );
    //Borrar esto !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    this.eliminarFichajeDeArray(user_id, fichaje_id);
    Swal.fire("Fichaje eliminado", "Fichaje eliminado", 'success');


  }
  //Elimina un fichaje de un usuario 
  eliminarFichajeDeArray(user_id: string, fichaje_id: string) {
    var fichajes: Fichaje[] = this.fichajes.get(user_id);

    for (let i = 0; fichajes.length > i; i++) {
      if (fichajes[i]._id === fichaje_id) {
        fichajes.splice(i, 1);
      }
    }
    this.fichajes.set(user_id, fichajes);

    //Recalculamos las estadisticas, probablemente salga NaN porque estas dejando un fichaje sin pareja es decir sin entrada o salida
    let hora = this.estadisticasService.calcularHorasTotales(this.fichajes.get(user_id));
    this.actualizarEstadisticas(user_id, hora);
  }
  //Actualiza un fichaje de un usuario
  actualizarFichajeDeArray(user_id: string, fichaje: Fichaje) {
    let fichajes: Fichaje[] = this.fichajes.get(user_id);

    for (let i = 0; fichajes.length > i; i++) {
      if (fichajes[i]._id === fichaje._id) {
        fichajes.splice(i, 1, fichaje);
      }
    }
    this.fichajes.set(user_id, fichajes);

    //Recalculamos las estadisticas
    let hora = this.estadisticasService.calcularHorasTotales(this.fichajes.get(user_id));
    this.actualizarEstadisticas(user_id, hora);
  }
  //Actualiza fechaInicial y fechaFinal si es necesario
  actualizarFechas(dateInicial: Date, dateFinal: Date) {
    if (this.dateInicial.getTime() !== dateInicial.getTime()) {
      this.dateInicial = dateInicial;

    }
    if (this.dateFinal.getTime() !== dateFinal.getTime()) {
      this.dateFinal = dateFinal;
    }

  }
  //Peticion GET fichajes
  cargarFichajesPeriodo(dateInicial: Date, dateFinal: Date) {
    //Actualiza las fechas si es necesario
    this.actualizarFechas(dateInicial, dateFinal);

    //Carga los fichajes de todos los usuarioSeleccionados
    this.usuariosSeleccionados.forEach(user_id => {
      this.fichajeService.cargarFichajesPeriodo(user_id, dateInicial, dateFinal)
        .subscribe(fichajes => {
          //Actualizamos los fichajes del usuario
          this.fichajes.set(user_id, fichajes);

          //Calculamos estadisticas del usuario
          let hora = this.estadisticasService.calcularHorasTotales(this.fichajes.get(user_id));
          this.actualizarEstadisticas(user_id, hora);
        }
        );
      //Borra !!!!!!!!!!!!!!!!!!!
      const nuevoArray3 = [new Fichaje(new Date(1800, 10, 5, 20, 10), 'entrada', { altitud: 23, latitud: 2 }, "60071a4458a35c903a179816", 'admin', "Doña Uzeada de Ribera Maldonado de Bracamonte y Anaya era baja, rechoncha, abigotada. Ya no existia razon para llamar talle al suyo. palmipedos domesticos. Sortijas celestes y azules le ahorcaban las"),
      new Fichaje(new Date(1700, 10, 2, 15, 22), 'salida', { altitud: 1, latitud: 1 }, "60071a4458a35c903a179817", 'admin')];
      //Borrar esto  !!!!!!!!!!!!
      this.fichajes.set(user_id, nuevoArray3);
      let hora = this.estadisticasService.calcularHorasTotales(this.fichajes.get(user_id));
      this.actualizarEstadisticas(user_id, hora);
    });

  }
  //Alerta para asegurar que quieres borrar un fichaje
  async modalEliminarFichaje(user_id: string, fichaje: Fichaje) {
    const dia = this.fechaPipe.transform(fichaje.date.getDate(), false);
    const mes = this.fechaPipe.transform(fichaje.date.getMonth(), true);
    const año = this.fechaPipe.transform(fichaje.date.getFullYear(), false);
    const hora = this.fechaPipe.transform(fichaje.date.getHours(), false);
    const minutos = this.fechaPipe.transform(fichaje.date.getMinutes(), false);

    Swal.fire({
      title: 'Desea eliminar el fichaje permanentemente',
      text: `Fichaje de ${fichaje.tipo} el  ${dia}/${mes}/${año} a las ${hora}:${minutos}`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Eliminar'
    }).then((result) => {
      if (result.isConfirmed) {
        //Eliminar el fichaje si usuario confirma
        this.eliminarFichaje(user_id, fichaje._id);
      }
    })
  }
  //Abrir el modal para actualizarFichaje
  modalActualizarFichaje(user_id: string, fichaje: Fichaje) {
    this.modalActualizarFichajeService.abrirModal(user_id, fichaje);
  }

  //Abrir el modal para filtrar otro periodo
  abrirModalFiltrarFichajes(user_id: string) {
    this.modalFiltrarFichajesService.abrirModal(user_id);
  }

  //Actualizamos horaTotales y minutosTotales
  actualizarEstadisticas(user_id: string, hora: string) {
    //Formato parametro hora= horas:minutos
    let pos = hora.indexOf(":");
    this.horasTotales.set(user_id, hora.slice(0, pos));
    this.minutosTotales.set(user_id, hora.slice(pos + 1, hora.length));
  }
}

