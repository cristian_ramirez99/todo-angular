import { Component, OnInit } from '@angular/core';
import { delay } from 'rxjs/operators';

import { Fichaje } from 'src/app/models/fichar.model';
import { Usuario } from 'src/app/models/usuario.model';
import { EstadisticasService } from 'src/app/services/estadisticas.service';
import { FichajeService } from 'src/app/services/fichaje.service';
import { ModalActualizarFichajeUsuarioService } from 'src/app/services/modal-actualizar-fichaje-usuario.service';
import { ModalFiltrarFichajesService } from 'src/app/services/modal-filtrar-fichajes.service';
import { ModalService } from 'src/app/services/modal.service';
import { UsuarioService } from 'src/app/services/usuario.service';


@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.css']
})
export class HistorialComponent implements OnInit {

  public fichajes: Fichaje[] = [];
  public usuario: Usuario;
  public horasTotales;
  public minutosTotales;

  public dateInicial: Date;
  public dateFinal: Date;

  constructor(private fichajeService: FichajeService,
    private usuarioService: UsuarioService,
    private modalService: ModalService,
    private modalActualizarFichajeUsuarioService: ModalActualizarFichajeUsuarioService,
    private modalFiltrarFichajesService: ModalFiltrarFichajesService,
    public estadisticasService: EstadisticasService,
  ) { this.usuario = this.usuarioService.usuario }

  ngOnInit(): void {
    //Por default fechaInicial=hace un mes y fechaFinal=hoy
    var date = new Date();

    //Hace un mes
    const fechaInicial = new Date(date.getFullYear(), date.getMonth() - 1, date.getDate());

    //Hoy
    const fechaFinal = new Date(date.getFullYear(), date.getMonth(), date.getDate());

    //Cargo los fichajes del ultimo mes 
    this.cargarFichajesPeriodo(fechaInicial, fechaFinal);

  }
  //Petcion GET de fichajes en periodo seleccionado
  cargarFichajesPeriodo(dateInicial: Date, dateFinal: Date) {
    //Actualizamos las nuevas fechas
    this.dateInicial = dateInicial;
    this.dateFinal = dateFinal;

    //Peticion GET de fichajes en el periodo
    this.fichajeService.cargarFichajesPeriodo(this.usuario.user_id, dateInicial, dateFinal)
      .subscribe(fichajes => {
        //Actualizamos fichajes
        this.fichajes = fichajes;
        
        //Calculamos estadisticas del periodo
        this.estadisticasService.calcularHorasTotales(this.fichajes);
        this.actualizarEstadisticas(hora);
      });
    this.fichajes.push(new Fichaje(new Date(1999, 9, 7, 20, 59), 'entrada', { altitud: 1, latitud: 2 }));
    this.fichajes.push(new Fichaje(new Date(1999, 8, 10, 1, 1), 'salida', { altitud: 1, latitud: 2 }));
    this.fichajes.push(new Fichaje(new Date(1999, 0, 20, 12, 21), 'entrada', { altitud: 41.3, latitud: 2.16 }, "", "", "En este fichaje estaba en el Oceano "));
    this.fichajes.push(new Fichaje(new Date(1999, 10, 20, 12, 10), 'salida', { altitud: 1, latitud: 2 }));
    this.fichajes.push(new Fichaje(new Date(1999, 10, 20, 10, 10), 'entrada', { altitud: 20.123, latitud: 20.12 }));
    this.fichajes.push(new Fichaje(new Date(1999, 10, 20, 12, 12), 'salida', { altitud: 1, latitud: 2 }));
    this.fichajes.push(new Fichaje(new Date(1999, 10, 20, 13, 13), 'entrada', { altitud: 1, latitud: 2 }));
    this.fichajes.push(new Fichaje(new Date(1999, 10, 20, 13, 15), 'salida', { altitud: 1, latitud: 2 }));
    this.fichajes.push(new Fichaje(new Date(1999, 10, 20, 20, 59), 'entrada', { altitud: 1, latitud: 2 }));
    this.fichajes.push(new Fichaje(new Date(1999, 10, 21, 1, 1), 'salida', { altitud: 1, latitud: 2 }));
    let hora = this.estadisticasService.calcularHorasTotales(this.fichajes);
    this.actualizarEstadisticas(hora);
  }
  //Actualizamos horaTotales y minutosTotales
  actualizarEstadisticas(hora: string) {
    //Formato parametro hora= horas:minutos
    let pos = hora.indexOf(":");
    this.horasTotales = hora.slice(0, pos);
    this.minutosTotales = hora.slice(pos + 1, hora.length);
  }
  historialVacio() {
    if (this.fichajes.length == 0) {
      return true;
    } else {
      return false;
    }
  }

  abrirModal(fichaje: Fichaje) {
    this.modalService.abrirModal(fichaje, false);
  }
  modalActualizarFichaje(fichaje: Fichaje) {
    this.modalActualizarFichajeUsuarioService.abrirModal(this.usuario.user_id, fichaje);
  }

  abrirModalFiltrarFichajes() {
    this.modalFiltrarFichajesService.abrirModal(this.usuario.user_id);
  }
}
