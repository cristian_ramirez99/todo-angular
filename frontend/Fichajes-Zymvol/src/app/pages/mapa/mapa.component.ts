import { Component, OnInit } from '@angular/core';
import { MapaService } from 'src/app/services/mapa.service';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent implements OnInit {

  constructor(private mapaService: MapaService) { }

  ngOnInit(): void {
    this.mapaService.buildMap();
  }

}
