import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2'

import { UsuarioService } from 'src/app/services/usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent {

  public formSubmitted = false;

  public loginForm = this.fb.group({
    user_id: ['', Validators.required],
    password: ['', Validators.required],
  });

  constructor(private router: Router,
    private fb: FormBuilder,
    private usuarioService: UsuarioService) { }

  //Si el campo esta vacio muestra error de ese campo
  campoNoValido(campo: string) {
    if (this.loginForm.get(campo).invalid && this.formSubmitted) {
      return true;
    } else {
      return false;
    }
  }

  //Peticion GET login
  login() {
    this.formSubmitted = true;

    //Si hay algun dato mal no hacemos peticion
    if (this.loginForm.invalid) {
      return;
    }

    //GET
    this.usuarioService.login(this.loginForm.value)
      .subscribe(resp => {
        if (resp.ok) {
          //Vamos al dashboard
          this.router.navigateByUrl('/');
        } else {
          //Si contraseña incorrecta 
          Swal.fire("Error", resp.msg, 'error');
        }
      }, (err) => {
        //Si usuario no existe en DB
        Swal.fire("Error", "No existe ese usuario", 'error');
      });
  }

}
