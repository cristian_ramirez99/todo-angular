import { Routes, RouterModule } from '@angular/router'
import { NgModule } from '@angular/core';

//Components
import { SignInComponent } from '../auth/sign-in/sign-in.component';
import { RegistrarComponent } from '../auth/registrar/registrar.component';
import { AuthGuard } from '../guards/auth.guard';
import { AdminGuard } from '../guards/admin.guard';


const routes: Routes = [
  { path: 'signIn', component: SignInComponent },

  //Rutas de admin
  { path: 'registrar', canActivate: [AuthGuard, AdminGuard], component: RegistrarComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
