import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2'
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent {

  public formSubmitted = false;

  roles: string[] = ["Usuario", "Administrador"];

  public registerForm = this.fb.group({
    user_id: ['', Validators.required],
    password: ['', Validators.required],
    rol: ['Usuario', Validators.required]
  });

  constructor(private fb: FormBuilder,
    private usuarioService: UsuarioService) { }

  //Si el campo esta vacio muestra error de ese campo
  campoNoValido(campo: string) {
    if (this.registerForm.get(campo).invalid && this.formSubmitted) {
      return true;
    } else {
      return false;
    }
  }
  //Peticion POST nuevoUsuario
  crearUsuario() {
    this.formSubmitted = true;

    //Si hay algun dato mal no hacemos peticion
    if (this.registerForm.invalid) {
      return;
    }

    //Realizar posteo
    this.usuarioService.crearUsuario(this.registerForm.value)
      .subscribe(resp => {
        //Registrado correctamente
        Swal.fire("Registrado", "Usuario registrado exitosamente", 'success');
      }, (err) => {
        Swal.fire("Error", "Usuario con ese nombre ya existe", 'error');
      });
  }

}
