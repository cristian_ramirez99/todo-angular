import { Injectable } from '@angular/core';
import { Fichaje } from '../models/fichar.model';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  private _ocultarModal: boolean = true;
  public addMarkerOnClick:boolean;

  public fichaje: Fichaje=null;

  get ocultarModal() {
    return this._ocultarModal;

  }
  abrirModal(fichaje: Fichaje, addMarkerOnClick: boolean) {
    this.fichaje = fichaje;
    this.addMarkerOnClick=addMarkerOnClick;
    this._ocultarModal = false;
  }
  cerrarModal() {
    this._ocultarModal = true;
  }
  constructor() { }
}
