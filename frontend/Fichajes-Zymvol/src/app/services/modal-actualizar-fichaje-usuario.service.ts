import { Injectable } from '@angular/core';
import { Fichaje } from '../models/fichar.model';

@Injectable({
  providedIn: 'root'
})
export class ModalActualizarFichajeUsuarioService {

  private _ocultarModal: boolean = true;
  public fichaje: Fichaje;
  public user_id: string;

  get ocultarModal() {
    return this._ocultarModal;

  }
  abrirModal(user_id: string, fichaje: Fichaje) {
    this.user_id = user_id;
    this.fichaje = fichaje;
    this._ocultarModal = false;
  }
  cerrarModal() {
    this._ocultarModal = true;
  }
  constructor() { }
}


