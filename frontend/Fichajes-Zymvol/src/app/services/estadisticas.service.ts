import { Injectable } from '@angular/core';
import { Fichaje } from '../models/fichar.model';

@Injectable({
  providedIn: 'root'
})
export class EstadisticasService {

  constructor() { }

  //Calcular las horas totales de los fichajes cargados 
  calcularHorasTotales(fichajes: Fichaje[]) {
    let auxHoras = 0;
    let auxMinutos = 0;

    let horaEntrada;
    let horaSalida;
    let minutoEntrada;
    let minutoSalida;

    let iterador = 0;

    fichajes.forEach(fichaje => {
      iterador++;
      //Entrada
      if (fichaje.tipo === 'entrada') {
        horaEntrada = fichaje.date.getHours();
        minutoEntrada = fichaje.date.getMinutes();
        //Salida
      } else {
        horaSalida = fichaje.date.getHours();
        minutoSalida = fichaje.date.getMinutes();
      }

      //Si he leido fichajeEntrada y fichajeSalida
      if (iterador % 2 == 0) {
        //Si el fichaje es en el mismo dia
        if (horaSalida - horaEntrada >= 0) {
          auxHoras += horaSalida - horaEntrada;
          //Si el fichaje es entre dos dias 
        } else {
          auxHoras += horaSalida - horaEntrada + 24;
        }
        //Diferencia de minutos entre fichajeEntrada y fichajeSalida
        auxMinutos += Math.abs(minutoSalida - minutoEntrada);
      }
    });
    /* Hacemos conversion para tenerlo en formato de horas habitual 
       devuelve horas:minutos */
    return this.conversionHora(auxHoras, auxMinutos);
  }

  private conversionHora(horas: number, minutos: number) {
    let cociente: number = Math.floor(minutos / 60);
    let residuo: number = minutos % 60;

    if (cociente > 0) {
      minutos = residuo;
      horas += cociente;
    }
    return horas + ':' + minutos;
  }

  //Calcula el promerdio de horas por dia
  calcularHorasAlDia(dateInicial: Date, dateFinal: Date, horasTotales: number) {
    var dif = dateFinal.getTime() - dateInicial.getTime();

    var dias = Math.floor(dif / (1000 * 60 * 60 * 24));

    //Media de horas por dia con un decimal
    return (horasTotales / dias).toFixed(1);
  }
}


