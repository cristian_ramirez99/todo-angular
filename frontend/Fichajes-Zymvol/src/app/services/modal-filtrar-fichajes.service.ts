import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalFiltrarFichajesService {

  private _ocultarModal: boolean = true;
  public user_id: string;

  get ocultarModal() {
    return this._ocultarModal;

  }
  abrirModal(user_id: string) {
    this.user_id = user_id;
    this._ocultarModal = false;
  }
  cerrarModal() {
    this._ocultarModal = true;
  }
  constructor() { }
}


