import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { Fichaje } from '../models/fichar.model';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class FichajeService {

  public fichaje: Fichaje = null;

  constructor(private http: HttpClient,
    private router: Router) { }

  get token() {
    return localStorage.getItem('token') || '';
  }
  get headers() {
    return {
      headers: {
        'x-token': this.token,
      }
    }
  }
  fichar(fichaje: Fichaje) {
    this.fichaje = fichaje;
    const url = `${base_url}/fichaje`;
    return this.http.post(url, fichaje, this.headers);
  }

  actualizarFichaje(fichaje: Fichaje) {
    const url = `${base_url}/fichaje/${fichaje._id}`;
    return this.http.put(url, fichaje, this.headers);
  }
  eliminarFichaje(fichaje_id: string) {
    const url = `${base_url}/fichaje/${fichaje_id}`;
    return this.http.delete(url, this.headers);
  }
  cargarFichajesPeriodo(user_id: string, dateInicial: Date, dateFinal: Date) {
    const url = `${base_url}/fichajesPeriodo/${user_id}/${dateInicial}/${dateFinal}`;
    return this.http.get(url, this.headers)
      .pipe(
        map((resp: { ok: boolean, fichajes: Fichaje[] }) => resp.fichajes)
      );
  }
  cargarUltimoFichaje(user_id: string) {
    const url = `${base_url}/ultimoFichaje/${user_id}`;
    return this.http.get(url, this.headers)
      .pipe(
        map((resp: { ok: boolean, fichaje: Fichaje }) => resp.fichaje)
      );

  }
}
