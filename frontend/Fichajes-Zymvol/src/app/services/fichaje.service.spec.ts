import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FichajeService } from './fichaje.service';
import { environment } from 'src/environments/environment';
import { Fichaje } from '../models/fichar.model';
import { Usuario } from '../models/usuario.model';

const base_url = environment.base_url;

describe('FichajeService', () => {
    let fichajeService: FichajeService;
    let httpTestingController: HttpTestingController;
    let routerTestingModule: RouterTestingModule;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, RouterTestingModule],
            providers: [FichajeService],
        });
    });
    beforeEach(() => {
        fichajeService = TestBed.get(FichajeService);
        httpTestingController = TestBed.get(HttpTestingController);
        routerTestingModule = TestBed.get(RouterTestingModule);

        let store = {};
        const mockLocalStorage = {
            getItem: (key: string): string => {
                return key in store ? store[key] : null;
            },
            setItem: (key: string, value: string) => {
                store[key] = `${value}`;
            },
            removeItem: (key: string) => {
                delete store[key];
            },
            clear: () => {
                store = {};
            }
        };
        spyOn(localStorage, 'getItem')
            .and.callFake(mockLocalStorage.getItem);
        spyOn(localStorage, 'setItem')
            .and.callFake(mockLocalStorage.setItem);
        spyOn(localStorage, 'removeItem')
            .and.callFake(mockLocalStorage.removeItem);
        spyOn(localStorage, 'clear')
            .and.callFake(mockLocalStorage.clear);
    });

    afterEach(() => {
        httpTestingController.verify();
    });

    it("Deberia eliminar un fichaje", () => {
        const fichaje: Fichaje = new Fichaje(new Date(1801, 11, 6, 21, 7), 'salida', { altitud: 5, latitud: 5 }, "60071a4458a35c903a179816", 'admin', "");

        fichajeService.eliminarFichaje(fichaje._id)
            .subscribe(resp => {
                expect(resp).toEqual(fichaje);
            });

        const req = httpTestingController.expectOne(base_url + '/fichaje/' + fichaje._id);

        expect(req.cancelled).toBeFalsy();
        expect(req.request.responseType).toEqual('json');

        //Ejecuta el subscribe
        req.flush(fichaje);
    });

    it("Deberia hacer un fichaje", () => {
        const fichaje: Fichaje = new Fichaje(new Date(1800, 10, 5, 20, 10), 'entrada', { altitud: 23, latitud: 2 }, "60071a4458a35c903a179816", 'admin', "");

        fichajeService.fichar(fichaje)
            .subscribe(resp => {
                expect(resp).toEqual(fichaje);
            });


        const req = httpTestingController.expectOne(base_url + '/fichaje');

        expect(req.cancelled).toBeFalsy();
        expect(req.request.responseType).toEqual('json');

        //Ejecuta el subscribe
        req.flush(fichaje);
    });

    it("Deberia actualizar un fichaje", () => {
        const fichaje: Fichaje = new Fichaje(new Date(1801, 11, 6, 21, 7), 'salida', { altitud: 5, latitud: 5 }, "60071a4458a35c903a179816", 'admin', "");

        fichajeService.actualizarFichaje(fichaje)
            .subscribe(resp => {
                expect(resp).toEqual(fichaje);
            });

        const req = httpTestingController.expectOne(base_url + '/fichaje/' + fichaje._id);

        expect(req.cancelled).toBeFalsy();
        expect(req.request.responseType).toEqual('json');

        //Ejecuta el subscribe
        req.flush(fichaje);

    });
     it("Deberia obtener los fichajes por periodos", () => {
         const usuario: Usuario = new Usuario("prueba500", 'Usuario', '50123');
         const fichaje: Fichaje[] = [new Fichaje(new Date(2021, 2, 6, 21, 7), 'entrada', { altitud: 5, latitud: 5 }, "60071a4458a35c903a179816", 'admin', ""),
         new Fichaje(new Date(2021, 2, 6, 22, 20), 'salida', { altitud: 5, latitud: 5 }, "60071a4458a35c903a179816", 'admin', "")];
 
         const dateInicial: Date = new Date(2021, 1, 1, 10, 10);
         const dateFinal: Date = new Date(2021, 10, 1, 10, 10);
 
         fichajeService.cargarFichajesPeriodo(usuario.user_id, dateInicial, dateFinal)
             .subscribe(resp => {
                 expect(resp).toEqual(fichaje);
             });
 
         const req = httpTestingController.expectOne(`${base_url}/fichajesPeriodo/${usuario.user_id}/${dateInicial}/${dateFinal}`);
 
         expect(req.cancelled).toBeFalsy();
         expect(req.request.responseType).toEqual('json');
 
         //Ejecuta el subscribe
         req.flush(fichaje);
 
     }); 
    /* it("Deberia cargar el ultimo fichaje", () => {
         let usuario: Usuario = new Usuario("prueba500", 'Usuario', '50123');
         const fichaje: Fichaje = new Fichaje(new Date(1801, 11, 6, 21, 7), 'salida', { altitud: 5, latitud: 5 }, "60071a4458a35c903a179816", 'admin', "");
 
         fichajeService.cargarUltimoFichaje(usuario.user_id)
             .subscribe(resp => {
                 expect(resp).toEqual(fichaje);
             });
 
         const req = httpTestingController.expectOne(base_url + '/ultimoFichaje/' + usuario.user_id);
 
         expect(req.cancelled).toBeFalsy();
         expect(req.request.responseType).toEqual('json');
 
         //Ejecuta el subscribe
         req.flush(fichaje);
     }); */
});