import { Injectable } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import { environment } from 'src/environments/environment';
import { Pos } from '../models/fichar.model';

const accesToken = environment.mapbox.accessToken;


@Injectable({
  providedIn: 'root'
})

export class MapaService {

  public markers: Pos[] = [];
  public currentMarkers: mapboxgl.Marker[] = [];
  
  map: mapboxgl.Map;
  style = 'mapbox://styles/mapbox/streets-v11';
  lat = 41.3879;
  lng = 2.16992;
  zoom = 6.5
  constructor() {
    Object.getOwnPropertyDescriptor(mapboxgl, "accessToken").set(accesToken);

  }
  buildMap() {
    this.markers = [];
    this.map = new mapboxgl.Map({
      container: 'map',
      style: this.style,
      zoom: this.zoom,
      center: [this.lng, this.lat],
    });
    this.map.addControl(new mapboxgl.NavigationControl());

  }


  addMarker(altitud: number, latitud: number) {
    if (this.markers.length == 0 || !this.isMarkerLoaded(altitud, latitud)) {
      var marker = new mapboxgl.Marker({})
        .setLngLat([altitud, latitud])
        .addTo(this.map);
      this.markers.push({ altitud, latitud });
      this.currentMarkers.push(marker);
    }
  }
  removeMarkers() {
    for (var i = this.currentMarkers.length - 1; i >= 0; i--) {
      this.currentMarkers[i].remove();
      this.markers.pop();
    }
  }
  private isMarkerLoaded(alt: number, lat: number) {
    for (let i = 0; this.markers.length > i; i++) {
      const altMarker = this.markers[i].altitud;
      const latMarker = this.markers[i].latitud;

      if (altMarker == alt && latMarker == lat) {
        return true;
      }
    }
    return false;
  }
  flying(altitud: number, latitud: number) {
    this.map.flyTo({
      center: [altitud, latitud],
      zoom: 9,
      essential: true
    });
  }

}