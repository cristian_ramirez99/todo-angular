from functools import wraps
from flask import Flask, request, jsonify, Response, make_response, url_for
from flask_pymongo import PyMongo
from flask_restful import abort, Api, Resource, reqparse
from bson import json_util
from authlib.integrations.flask_client import OAuth
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token, get_jwt_identity
)
from datetime import timedelta
from dotenv import load_dotenv
import os

# LoginManger
# login_manager = LoginManager()

# env
load_dotenv()

app = Flask(__name__)

# init LoginManager
# login_manager.init_app(app)
# login_manager.login_view = "login"

# Config db
app.config['MONGO_URI'] = "mongodb://db_v1/test"

# init pymongo
mongo = PyMongo(app)

# init Oauth
oauth = OAuth(app)

# init flask_restful
api = Api(app)

# Config SecretKey
app.config['SECRET_KEY'] = os.getenv("SECRET_KEY")

# JWT
app.config['JWT_SECRET_KEY'] = os.getenv("JWT_SECRET_KEY")
jwt = JWTManager(app)

# Config sessionDuration
app.permanent_session_lifetime = timedelta(hours=1)

# Config google Oauth
app.config['GOOGLE_CLIENT_ID'] = os.getenv("GOOGLE_CLIENT_ID")
app.config['GOOGLE_CLIENT_SECRET'] = os.getenv("GOOGLE_CLIENT_SECRET")
google = oauth.register(
    name='google',
    client_id=app.config["GOOGLE_CLIENT_ID"],
    client_secret=app.config["GOOGLE_CLIENT_SECRET"],
    access_token_url='https://accounts.google.com/o/oauth2/token',
    access_token_params=None,
    authorize_url='https://accounts.google.com/o/oauth2/auth',
    authorize_params=None,
    api_base_url='https://www.googleapis.com/oauth2/v1/',
    userinfo_endpoint='https://openidconnect.googleapis.com/v1/userinfo',
    # This is only needed if using openId to fetch user info
    client_kwargs={'scope': 'openid email profile'},
)


def build_preflight_response():
    response = make_response()
    # Esta la añadimos siempre en corrs_ready
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add('Access-Control-Allow-Headers', "*")
    # response.headers.add('Access-Control-Allow-Methods', "*")
    # response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    response.headers.add('Access-Control-Allow-Credentials', 'true')

    return response


def build_cors_ready_response(response):
    print("adding cors ready", flush=True)
    print(request.method, flush=True)
    if request.method != "OPTIONS":
        response.headers["Access-Control-Allow-Origin"] = "*"
    return response


def cors_ready(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        print(args, flush=True)
        print(kwargs, flush=True)
        method_response = func(*args, **kwargs)
        print("method response")
        print(method_response)
        print(method_response.headers, flush=True)
        response = build_cors_ready_response(method_response)
        print("processed response")
        print(response, flush=True)
        print(response.headers, flush=True)
        return response

    return wrapper


class CORSReadyResource(Resource):
    method_decorators = [cors_ready]

    def options(self):
        print("options asked", flush=True)
        print(str(request))
        response = build_preflight_response()
        print(response.headers)
        return response


def error_handler(message, status):
    response = jsonify({'message': message + " " + request.url,
                        'status': status})
    response.status_code = status
    return abort(response)


def abort_if_user_id_exist(user_id):
    user = mongo.db.users.find_one({'user_id': user_id})
    if user is not None:
        error_handler("user already exist", 409)


def abort_if_user_id_doesnt_exist(user_id):
    user = mongo.db.users.find_one({'user_id': user_id})
    if user is None:
        error_handler("user doesn't exist", 404)


# Google login route
@app.route('/login/google')
def google_login():
    google = oauth.create_client('google')
    redirect_uri = url_for('google_authorize', _external=True)
    return google.authorize_redirect(redirect_uri)


# Google authorize route
@app.route('/login/google/authorize')
def google_authorize():
    google = oauth.create_client('google')
    token = google.authorize_access_token()
    response = google.get('userinfo').json()
    print(f"\n{response}\n")
    return "You are sucessfully signed in using google"


def getCurrentUserAndRol():
    current_user = get_jwt_identity()
    abort_if_user_id_doesnt_exist(current_user)
    current_user_rol = mongo.db.users.find_one({'user_id': current_user}, {'_id': 0, 'rol': 1})
    current_user_rol = current_user_rol['rol']
    return current_user, current_user_rol


# class Login
class Login(CORSReadyResource):
    def post(self):
        user_id = request.json['user_id']

        abort_if_user_id_doesnt_exist(user_id)

        password = request.json['password']
        jsonPassword = mongo.db.users.find_one({'user_id': user_id}, {'_id': 0, 'password': 1})
        passwordDB = jsonPassword['password']

        if (password == passwordDB):
            token_expires = timedelta(hours=2)
            token = create_access_token(identity=user_id, expires_delta=token_expires)
            response = json_util.dumps({
                'ok': True,
                'token': token
            })
        else:
            response = json_util.dumps({
                'ok': False,
                'msg': 'La contraseña es incorrecta'
            })

        return Response(response, mimetype='application/json')

    @jwt_required
    def get(self):
        current_user = get_jwt_identity()

        abort_if_user_id_doesnt_exist(current_user)
        user = mongo.db.users.find_one({'user_id': current_user}, {'_id': 0, 'password': 0})

        token_expires = timedelta(hours=2)
        token = create_access_token(identity=current_user, expires_delta=token_expires)

        response = json_util.dumps({
            'ok': True,
            'usuario': user,
            'token': token
        })

        return Response(response, mimetype='application/json')


# Clase User
class UsersList(CORSReadyResource):
    @jwt_required
    def get(self):
        current_user, current_user_rol = getCurrentUserAndRol()

        if current_user_rol == 'Administrador':
            users = mongo.db.users.find({'password': {'$exists': 'true'}})
            response = json_util.dumps({
                "ok": True,
                "users": users
            })
            return Response(response, mimetype='application/json')
        else:
            response = json_util.dumps({
                "ok": False,
                "message": "No tienes permiso para hacer esto"
            })
            return Response(response, mimetype='application/json')


# Clase User
class User(CORSReadyResource):
    @jwt_required
    def get(self, user_id):

        current_user, current_user_rol = getCurrentUserAndRol()

        if current_user == user_id or current_user_rol == 'Administrador':
            user = mongo.db.users.find({'user_id': user_id})

            response = json_util.dumps({
                "ok": True,
                "user": user
            })
            return Response(response, mimetype='application/json')
        else:
            response = json_util.dumps({
                "ok": False,
                "message": "No tienes permiso para hacer esto"
            })
            return Response(response, mimetype='application/json')

    @jwt_required
    def post(self):
        current_user, current_user_rol = getCurrentUserAndRol()

        if current_user_rol == 'Administrador':
            user_id = request.json['user_id']
            abort_if_user_id_exist(user_id)

            password = request.json['password']
            rol = request.json['rol']

            if user_id and password:
                mongo.db.users.insert_one({'user_id': user_id,
                                           'password': password,
                                           'rol': rol})

                response = json_util.dumps({
                    'ok': True
                })

                return Response(response, mimetype='application/json')

            else:
                return error_handler("Resource doesn't exist", 409)
        else:
            response = json_util.dumps({
                'ok': False,
                'message': 'No tienes permiso para hacer esto'
            })
            return Response(response, mimetype='application/json')

    @jwt_required
    def delete(self, user_id):
        current_user, current_user_rol = getCurrentUserAndRol()

        if current_user_rol == 'Administrador':
            abort_if_user_id_doesnt_exist(user_id)

            mongo.db.users.delete_many({'user_id': user_id})

            response = json_util.dumps({
                'ok': True,
                'message': 'User has been deleted successfully'
            })
        else:
            response = json_util.dumps({
                'ok': False,
                'message': 'No tienes permiso para hacer esto'
            })

        return Response(response, mimetype='application/json')


# Clase Fichaje
class UltimoFichaje(CORSReadyResource):
    @jwt_required
    def get(self, user_id):
        abort_if_user_id_doesnt_exist(user_id)

        current_user, current_user_rol = getCurrentUserAndRol()

        if (current_user == user_id):

            fichaje = mongo.db.fichajes.find({'user_id': user_id}).sort('date', -1).limit(1)

            response = json_util.dumps({
                "ok": True,
                "fichaje": fichaje
            })
        else:
            response = json_util.dumps({
                "ok": False,
                "message": "Algo extraño sucedio pregunte al administrador"
            })
        return Response(response, mimetype='application/json')


# Clase Fichaje
class FichajesPeriodo(CORSReadyResource):
    @jwt_required
    def get(self, user_id, dateInicial, dateFinal):
        abort_if_user_id_doesnt_exist(user_id)

        current_user, current_user_rol = getCurrentUserAndRol()

        if (current_user_rol == 'Administrador' or current_user == user_id):

            fichajes = mongo.db.fichajes.find({'user_id': user_id, "date": {"$gte": dateInicial, "$lt": dateFinal}})

            response = json_util.dumps({
                "ok": True,
                "fichajes": fichajes
            })
        else:
            response = json_util.dumps({
                "ok": False,
                "message": "No tienes permiso para hacer eso"
            })
        return Response(response, mimetype='application/json')


# Clase Fichaje
class Fichaje(CORSReadyResource):
    @jwt_required
    def get(self, user_id, fichaje_id):
        current_user, current_user_rol = getCurrentUserAndRol()

        if current_user == user_id or current_user_rol == 'Administrador':
            abort_if_user_id_doesnt_exist(user_id)

            fichaje = mongo.db.fichajes.find_one({'_id': fichaje_id},
                                                 {'user_id': 0,
                                                  '_id': 0,
                                                  })
            response = json_util.dumps({
                "ok": True,
                "fichaje": fichaje
            })
        else:
            response = json_util.dumps({
                "ok": False,
                "message": "No tienes permiso para hacer esto"
            })
        return Response(response, mimetype='application/json')

    @jwt_required
    def post(self):
        current_user = get_jwt_identity()

        # user_id = request.json['user_id']
        abort_if_user_id_doesnt_exist(current_user)

        date = request.json['date']
        tipo = request.json['tipo']

        # siendo pos :{'altitud':altitud,'latitud':"latitud}
        pos = request.json['pos']

        mongo.db.fichajes.insert_one({'user_id': current_user,
                                      'date': date,
                                      'tipo': tipo,
                                      'pos': pos,
                                      })

        response = json_util.dumps({
            'ok': True,
            'message': 'User was updated succesfully'
        })
        return Response(response, mimetype='application/json')

    @jwt_required
    def delete(self, fichaje_id):
        current_user, current_user_rol = getCurrentUserAndRol()

        if current_user_rol == 'Administrador':
            mongo.db.fichajes.delete_one({'_id': fichaje_id})

            response = json_util.dumps({
                'ok': True,
            })
        else:
            response = json_util.dumps({
                'ok': False,
                'message': 'No tiene permiso para hacer eso'
            })
        return Response(response, mimetype='application/json')

    @jwt_required
    def put(self, fichaje_id):
        current_user, current_user_rol = getCurrentUserAndRol()

        if current_user_rol == 'Administrador':

            date = request.json['date']
            tipo = request.json['tipo']
            pos = request.json['pos']

            mongo.db.fichajes.update_one({'_id': fichaje_id}, {'$set': {'date': date, 'tipo': tipo, 'pos': pos}})

            response = json_util.dumps({
                'ok': True,
            })
        else:
            response = json_util.dumps({
                'ok': False,
                'message': 'No tiene permiso para hacer eso'
            })
        return Response(response, mimetype='application/json')


# post ->login get ->renew token
api.add_resource(Login, '/api/login')

api.add_resource(UsersList, '/api/users')

api.add_resource(User, '/api/user', '/api/user/<string:user_id>')

api.add_resource(FichajesPeriodo, '/api/fichajesPeriodo/<string:user_id>/<string:dateInicial>/<string:dateFinal>')

api.add_resource(Fichaje, '/api/fichaje', '/api/fichaje/<ObjectId:fichaje_id>')

api.add_resource(UltimoFichaje, '/api/ultimoFichaje/<string:user_id>')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=os.getenv("PORT"), debug=True)
